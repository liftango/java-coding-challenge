package app;

import java.util.Random;

import static spark.Spark.*;

public class Application {

    public static void main(String[] args) {

        Adder adder = new Adder();

        port(4567);

        get("/ping", (req, res) -> {
           return "Hello, World!";
        });

        get("/calc", (req, res) -> {
            int inputNumber = new Random().nextInt(10);
            return adder.add(inputNumber);
        });

    }

}
