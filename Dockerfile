FROM adoptopenjdk/openjdk11:jdk-11.0.6_10-alpine-slim
COPY ./build/libs/assessment-1.0-SNAPSHOT.jar /usr/app/
WORKDIR /usr/app
EXPOSE 4567
ENTRYPOINT ["java", "-jar", "assessment-1.0-SNAPSHOT.jar"]